# Music Genre Classification

Music genre classification using deep learning models on GTZAN and Free Music Archive datasets.

- Genre classification is done on GTZAN(1000 audio files/10 genres) and Free Music Archive(FMA)(8000 audio files/8 genres) dataset.
- The audio file is converted into Mel-frequency cepstral coefficients(MFCCs) and Mel-Spectrograms using the python librosa library.
- Classification is done using the following models:
    - Fully Connected Neural Networks
    - RNN using LSTM
    - CNN-RNN(CRNN) (To Do)
    - Parallel CNN-RNN (To Do)
- Models are evaluated and compared using Accuracy and F1-score metrics. 

## Mel-frequency cepstral coefficients (MFCCs)
- MFCCs are compressible representations of a mel-spectrogram.
- These coefficients are set of features, which can be used to determine the shape of frequency spectrum.

## Mel-Spectrograms
- A visual way to represent signal strength of different frequencies over time.

## Fully Connected Neural Network
- Each neuron is connected to every other neuron in the previous layer and each connection has its own weight.
- It also makes no assumption about the features in the data.
- Model has 4 Dense layers with Relu activation function and glorot uniform initializer.
- Each dense layer has dropouts of 0.4 and batch normalization.
- The output layer has the softmax activation function.
- RMSprop optimizer and categorical cross entropy loss function is used.

Colab Link - https://colab.research.google.com/drive/1bN6V_jtkRkFjK_DVrsm6UBeP0YYH8-xu?usp=sharing

## RNN-LSTM
- LSTM is a type of RNN that works well in sequential data like music and speech.
- It avoids long-term dependencies problem.
- The model has 2 LSTM layers, the first layer shape as per the input shape.
- A Dense layer following the LSTM layer with Relu activation function.
- Followed by a dropout of 0.3 and the output layer with softmax activation function.
- Model uses RMSprop optimizer and categorical cross entropy loss function. 



Colab Link - https://colab.research.google.com/drive/1m5sVF0eKpmvlCPcf-QPvSWL-supuTrxm?usp=sharing

